terraform {
  required_providers {
    ignition = {
      source = "community-terraform-providers/ignition"
      version = "2.2.1"
    }
  }
}
