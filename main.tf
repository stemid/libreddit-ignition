data "ignition_file" "environment" {
  path = "/home/${var.username}/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {})
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_file" "container_unit" {
  path = "/home/${var.username}/.config/containers/systemd/libreddit.container"
  content {
    content = templatefile("${path.module}/templates/libreddit.container", {
      username = var.username
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.environment.rendered,
    data.ignition_file.container_unit.rendered,
  ]
}
